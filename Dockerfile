FROM datastax/dse-server:6.8.34

COPY init-database.sh /init-database.sh

COPY create-keyspace.cql /create-keyspace.cql
COPY create-tables.cql /create-tables.cql

USER root
RUN chmod +x /init-database.sh
USER dse
