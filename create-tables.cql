CREATE KEYSPACE highsub WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3'}
                         AND durable_writes = true;
CREATE TABLE highsub.plex_openings
(
    show        text,
    season      text,
    episode     text,
    marker_type text,
    id          int,
    end         bigint,
    extra_data  text,
    start       bigint,
    PRIMARY KEY ((show, season, episode), marker_type, id)
) WITH CLUSTERING ORDER BY (marker_type ASC, id ASC);

CREATE TABLE highsub.episode
(
    show                    text,
    season                  text,
    episode                 text,
    duration                bigint,
    ending_end              bigint,
    ending_start            bigint,
    english_subtitles       text,
    hashes                  map<text, text>,
    height                  int,
    is_anime                boolean static,
    japanese_subtitles      text,
    kitsu_anime_id          text static,
    opening_end             bigint,
    opening_start           bigint,
    originally_available_at text,
    parent_tags             list<text>,
    preview_end             bigint,
    preview_start           bigint,
    prologue_end            bigint,
    prologue_start          bigint,
    scaled_150p_video       text,
    scaled_150p_video_bytes bigint,
    scaled_480p_video       text,
    scaled_480p_video_bytes bigint,
    show_year               int static,
    source_video            text,
    source_video_bytes      bigint,
    studio                  text,
    subtitle_count          bigint,
    summary                 text,
    tags                    list<text>,
    title                   text,
    tmdb_episode            int,
    tmdb_episode_group      text static,
    tmdb_episode_id         int,
    tmdb_season             int,
    tmdb_show_id            int static,
    video_file              text,
    width                   int,
    PRIMARY KEY (show, season, episode)
) WITH CLUSTERING ORDER BY (season ASC, episode ASC);

CREATE TABLE highsub.show
(
    show               text PRIMARY KEY,
    is_anime           boolean,
    kitsu_anime_id     text,
    plex_key           text,
    show_year          int,
    summary            text,
    tags               list<text>,
    tmdb_episode_group text,
    tmdb_show_id       int
);

CREATE TABLE highsub.collection_content
(
    subtitles   frozen<list<text>> PRIMARY KEY,
    created_utc bigint,
    id          uuid
);

CREATE TABLE highsub.hashed_subtitles
(
    show            text,
    hash            text,
    episode         text,
    season          text,
    subtitle_number int,
    PRIMARY KEY (show, hash)
) WITH CLUSTERING ORDER BY (hash ASC);

CREATE TABLE highsub.content_hit
(
    show     text,
    season   text,
    episode  text,
    subtitle text,
    type     text,
    hit_utc  bigint,
    PRIMARY KEY ((show, season, episode, subtitle), type, hit_utc)
) WITH CLUSTERING ORDER BY (type ASC, hit_utc ASC);

CREATE TABLE highsub.flat_subtitle
(
    show                     text,
    season                   text,
    episode                  text,
    subtitle_number          int,
    context                  list<text>,
    created_utc              bigint,
    end_time                 bigint,
    japanese_end_time        bigint,
    japanese_start_time      bigint,
    japanese_subtitle_number int,
    kanji                    text,
    match_rate               text,
    name                     text,
    parent_tags              list<text>,
    raw_subtitle             list<text>,
    romaji                   text,
    start_time               bigint,
    style                    text,
    tags                     list<text>,
    text                     text,
    PRIMARY KEY ((show, season, episode), subtitle_number)
) WITH CLUSTERING ORDER BY (subtitle_number ASC);

CREATE TABLE highsub.for_scrape
(
    url    text PRIMARY KEY,
    loaded boolean,
    show   text
);

CREATE TABLE highsub.actor_role_rule
(
    actor     text,
    character text,
    show      text,
    type      text,
    option    text,
    PRIMARY KEY (actor, character, show, type)
) WITH CLUSTERING ORDER BY (character ASC, show ASC, type ASC);

CREATE TABLE highsub.saved_event
(
    domain     text,
    id         text,
    event_utc  bigint,
    event_json text,
    PRIMARY KEY ((domain, id), event_utc)
) WITH CLUSTERING ORDER BY (event_utc ASC);

CREATE TABLE highsub.actor_role
(
    show               text,
    actor              text,
    character          text,
    actor_image        text,
    character_image    text,
    character_role     text,
    kitsu_character_id text,
    kitsu_person_id    text,
    source             text,
    PRIMARY KEY (show, actor, character)
) WITH CLUSTERING ORDER BY (actor ASC, character ASC);
CREATE CUSTOM INDEX actor_sai ON highsub.actor_role (actor) USING 'StorageAttachedIndex';

ALTER TABLE highsub.flat_subtitle add chapter text;
ALTER TABLE highsub.flat_subtitle add should_index boolean;


CREATE TABLE highsub.vector_cache 
(
    version     text,
    input       text,
    vector      list<double>,
    cache_time_utc   bigint,
    PRIMARY KEY (version,input)
);

ALTER TABLE highsub.flat_subtitle add type text;

ALTER TABLE highsub.content_hit ADD content_type text;
ALTER TABLE highsub.content_hit ADD content_height int;
ALTER TABLE highsub.content_hit ADD content_start_time bigint;
ALTER TABLE highsub.content_hit ADD content_end_time bigint;
ALTER TABLE highsub.content_hit ADD hit_type text;
ALTER TABLE highsub.content_hit ADD focused boolean;
ALTER TABLE highsub.content_hit add user text;
ALTER TABLE highsub.content_hit add user_agent text;
ALTER TABLE highsub.content_hit add ip text;
ALTER TABLE highsub.content_hit add content_key text;
ALTER TABLE highsub.content_hit add content_path text;

CREATE TABLE highsub.search
(
    id                         uuid,
    query                      text,
    page                       int,
    size                       int,
    safe_search                text,
    type                       text,
    search_criteria            text,
    user_agent                 text,
    ip                         text,
    user                       text,
    context                    text,
    show                       text,
    blacklist_shows            list<text>,
    latency                    bigint,
    timestamp                  bigint,
    PRIMARY KEY (id)
);

CREATE TABLE highsub.search_result
(
    search_id                uuid,
    rank                     int,
    show                     text,
    season                   text,
    episode                  text,
    subtitle_number          int,
    context                  list<text>,
    created_utc              bigint,
    end_time                 bigint,
    japanese_end_time        bigint,
    japanese_start_time      bigint,
    japanese_subtitle_number int,
    kanji                    text,
    match_rate               text,
    name                     text,
    parent_tags              list<text>,
    raw_subtitle             list<text>,
    romaji                   text,
    start_time               bigint,
    style                    text,
    tags                     list<text>,
    text                     text,
    chapter                  text,
    should_index             boolean,
    type                     text,
    PRIMARY KEY (search_id, rank)
) WITH CLUSTERING ORDER BY (rank ASC);

ALTER TABLE highsub.show add season_name_map map<text,text>;
ALTER TABLE highsub.episode add season_name text;
ALTER TABLE highsub.episode add slug text;
ALTER TABLE highsub.episode add preview_subtitle_id text;
ALTER TABLE highsub.episode add indexed_subtitle_count bigint;
ALTER TABLE highsub.flat_subtitle add episode_width int;
ALTER TABLE highsub.flat_subtitle add episode_height int;
ALTER TABLE highsub.flat_subtitle add episode_slug text;
ALTER TABLE highsub.search_result add episode_width int;
ALTER TABLE highsub.search_result add episode_height int;
ALTER TABLE highsub.search_result add episode_slug text;
