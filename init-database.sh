#!/usr/bin/env bash
/entrypoint.sh
dse cassandra -f &
DATASTAX_PID=$!
echo "Waiting 40 seconds for cassandra to start"
sleep 40

LIVENESS_CQL="SELECT release_version FROM system.local"
until  echo "$LIVENESS_CQL" | cqlsh ; do
  >&2 echo "Cassandra is unavailable - sleeping"
  sleep 2
done
cqlsh -f /create-keyspace.cql
cqlsh -f /create-tables.cql

kill $DATASTAX_PID
